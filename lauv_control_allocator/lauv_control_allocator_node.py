from cmath import pi
import rclpy
from rclpy.node import Node
import tf2_py as tf2
import tf2_ros

import numpy as np
import casadi

from plankton_utils.time import is_sim_time
from plankton_utils.time import time_in_float_sec
from uuv_auv_control_allocator.msg import AUVCommand
from uuv_thrusters.models import Thruster
from lauv_control_allocator.fin_model import FinModel

import threading

class LauvControlAllocator(Node):
    NUMBER_FINS = 4
    
    def __init__(self, **kwargs):
        super().__init__('lauv_control_allocator_node',
                        allow_undeclared_parameters=True, 
                        automatically_declare_parameters_from_overrides=True,
                        **kwargs)
        
        self.namespace = "lauv"
        print("name_space is:", self.namespace)
        self.get_logger().info('Initialize control allocator for vehicle <%s>' % self.namespace) 
        
        ## Fins param
        self.fins = dict()
        self.n_fins = 0
        self.fin_config = dict()
        self.fin_config['fin_lower_limit'] = -1.396263402
        self.fin_config['fin_upper_limit'] = 1.396263402
        self.fin_config['fin_area'] = 0.0064
        self.fin_config['lift_coefficient'] = 3.0
        self.fin_config['fluid_density'] = 1028.0
        
        ## Thruster param
        self.n_thruster = 0
        self.thruster_config = dict()
        self.thruster_config['max_thrust'] = 20.0
        
        self.found_actuator = False
        self.update_rate = 10.0
        self.timeout = 1.0
        
        # # Control allocation matrix for thruster and fin lift forces
        self.u = casadi.SX.sym('u')
        self.thrust = casadi.SX.sym('thrust')
        self.delta = None
        self.output_tau = casadi.SX.sym('tau', 6, 1)
        self.actuator_model = 0
        
        self.input_surge_speed = 0.0
        self.input_tau = np.zeros(6)    
        
        ## Find Actuators
        self.tf_buffer = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tf_buffer, self)
        tf_trans_ned_to_enu = None
        
        # Create list of thruster and fins command
        self.timer = self.create_timer(1.0, self.find_actuators)
        

        ## Wait until actuator found
        rate = self.create_rate(self.update_rate)
        thread = threading.Thread(target=rclpy.spin, args=(self,), daemon=True)
        thread.start()
        while rclpy.ok() and not self.found_actuator:
            rate.sleep()
            
        self.get_logger().info("################ START NODE ##############")
        
        ## Create force command subscription
        if not self.init():
            raise RuntimeError('No thruster and/or fins found')
        self.sub = self.create_subscription(AUVCommand, 'control_input', self.control_callback, 10)
        self.last_update = time_in_float_sec(self.get_clock().now())

        while rclpy.ok():
            if self.timeout > -1:
                if time_in_float_sec(self.get_clock().now()) - self.last_update > self.timeout:
                    self.input_surge_speed = 0.0
                    self.input_tau = np.zeros(6)            
            output = self.allocate(self.input_tau, self.input_surge_speed)            
            self.publish_commands(output)
            rate.sleep()
        # self.control_timer = self.create_timer(1.0, self.control_callback)
        
    def allocate(self, tau, u):        
        output = np.zeros(self.n_fins + 1)

        if self.input_surge_speed == 0:
            return output

        if np.linalg.norm(tau) == np.abs(tau[0]):
            output[0] = self.thruster.tam_column[0] * tau[0]
        else:
            model = casadi.substitute(
                self.cost_function, 
                casadi.vertcat(self.output_tau, self.u), 
                tau.tolist() + [u])
                
            nlp = dict(
                x=casadi.vertcat(self.thrust, self.delta),
                f=model)

            opts = {'verbose':False, 'print_time':False, 'ipopt.print_level':0}

            solver = casadi.nlpsol('solver', 'ipopt', nlp, opts)
            sol = solver(
                lbx=[-self.thruster_config['max_thrust']] + [self.fin_config['fin_lower_limit'] for _ in range(self.n_fins)],
                ubx=[self.thruster_config['max_thrust']] + [self.fin_config['fin_upper_limit'] for _ in range(self.n_fins)])
            
            for i in range(self.n_fins + 1):
                output[i] = sol['x'][i]
        return output
    
    def init(self):
        """Calculate the control allocation matrix, if one is not given."""
                
        # Build the casadi model
        self.delta = casadi.SX.sym('delta', self.n_fins)
        actuator_model = self.thruster.tam_column.reshape((6, 1)) * self.thrust
        for i in self.fins:
            f_lift = (0.5 * self.fin_config['fluid_density'] * 
                self.fin_config['lift_coefficient'] * self.fin_config['fin_area'] *  
                self.delta[i] * self.u**2)
        
            f_lift = f_lift * self.fins[i].lift_vector
            t_lift = casadi.cross(self.fins[i].pos, f_lift)

            actuator_model += casadi.vertcat(f_lift, t_lift)

        self.cost_function = casadi.norm_2(self.output_tau - actuator_model)
        
        return True
    
    def publish_commands(self, command):
        self.thruster.publish_command(command[0])

        for i in range(self.n_fins):
            self.fins[i].publish_command(command[i + 1])
        
    def find_actuators(self):
        """Calculate the control allocation matrix, if one is not given."""
        
        self.get_logger().info('ControlAllocator: updating thruster poses')

        base = '%s/%s' % (self.namespace, "base_link")

        # frame = '%s/%s%d' % (self.namespace, "thruster_", 0)
        frame = 'lauv/thruster_0'
        
        thruster_topic = "/lauv/thrusters/id_0/input"
        
        try:
            self.get_logger().info('Lookup: Thruster transform found %s -> %s' % (base, frame))
            now = self.get_clock().now()
            trans = self.tf_buffer.lookup_transform(base, frame, rclpy.time.Time(), rclpy.time.Duration(seconds=0.1))
            pos = np.array([trans.transform.translation.x,
                            trans.transform.translation.y,
                            trans.transform.translation.z])
            quat = np.array([trans.transform.rotation.x,
                                trans.transform.rotation.y,
                                trans.transform.rotation.z,
                                trans.transform.rotation.w])
            self.get_logger().info('Thruster transform found %s -> %s' % (base, frame))
            self.get_logger().info('pos=' + str(pos))
            self.get_logger().info('rot=' + str(quat))
            
            # params = {key: val.value for key, val in params.items()}
            self.thruster = Thruster.create_thruster( self,
            'proportional', 0, 
            thruster_topic, pos, quat, 
            **{"gain" : 0.000049})
            
            self.n_thruster = 1
        
        except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException):
            self.get_logger().info('Could not get transform from %s to %s ' % (base, frame))
            
        try:
            for i in range(self.NUMBER_FINS):
                frame = '%s/%s%d' % (self.namespace, 'fin', i)
                
                self.get_logger().info('Lookup: Fin transform found %s -> %s' % (base, frame))
                trans = self.tf_buffer.lookup_transform(base, frame, rclpy.time.Time(), rclpy.time.Duration(seconds=0.1))
                pos = np.array([trans.transform.translation.x,
                                   trans.transform.translation.y,
                                   trans.transform.translation.z])
                quat = np.array([trans.transform.rotation.x,
                                    trans.transform.rotation.y,
                                    trans.transform.rotation.z,
                                    trans.transform.rotation.w])                
                self.get_logger().info('Fin transform found %s -> %s' % (base, frame))
                self.get_logger().info('pos=' + str(pos))
                self.get_logger().info('quat=' + str(quat))

                fin_topic = self.build_topic_name(self.namespace, 
                    'fins', i, 'input')
                
                print(fin_topic)
                print(i)

                self.fins[i] = FinModel(
                    i,
                    pos,
                    quat,
                    fin_topic,
                    self)

        except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException):
            self.get_logger().info('Could not get transform from %s to %s ' % (base, frame))
            # break

        self.n_fins = len(self.fins.keys())
        self.get_logger().info('# fins found: %d' % len(self.fins.keys()))
        
        if (self.n_thruster == 1 and self.n_fins == 4):
            self.get_logger().info('found %s thrsuter and %s fins' % (self.n_thruster, self.n_fins))
            self.found_actuator = True
            self.get_logger().info('destroy timer')
            self.timer.destroy()

    def build_topic_name(self, namespace, topic_prefix, id, topic_suffix):
        return '/%s/%s/id_%d/%s' %  (namespace, topic_prefix, id, topic_suffix)
    
    def control_callback(self, msg):
        self.input_surge_speed = msg.surge_speed
        self.input_tau = np.array([msg.command.force.x, 
                                   msg.command.force.y,
                                   msg.command.force.z,
                                   msg.command.torque.x, 
                                   msg.command.torque.y,
                                   msg.command.torque.z])  

        if 'ned' in msg.header.frame_id:
            self.input_tau[0:3] = np.dot(self.base_link_ned_to_enu, self.input_tau[0:3])
            self.input_tau[4::] = np.dot(self.base_link_ned_to_enu, self.input_tau[4::])
        
        self.last_update = time_in_float_sec(self.get_clock().now())

# =============================================================================
def main(args=None):
    rclpy.init(args=args)

    sim_time_param = is_sim_time()
    print("sim_time_param:", sim_time_param.get_parameter_value().bool_value)


    node = LauvControlAllocator(parameter_overrides=[sim_time_param])
    # node = LauvControlAllocator()
    
    rclpy.spin(node)
    
    node.destroy_node()
    rclpy.shutdown()

# =============================================================================
if __name__ == '__main__':
    main()
        