from setuptools import setup

package_name = 'lauv_control_allocator'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='jay',
    maintainer_email='vasutorn.s@ku.th',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'lauv_control_allocator_node = lauv_control_allocator.lauv_control_allocator_node:main',
            'wait_transform = lauv_control_allocator.wait_transform:main'
        ],
    },
)
